var main__MotorCtrl_8py =
[
    [ "closedloop", "main__MotorCtrl_8py.html#a4ddd10cb39358c9d40c3b6afd727d030", null ],
    [ "counter", "main__MotorCtrl_8py.html#ad3760a22f4d08f102b6380deb575afc1", null ],
    [ "encoder", "main__MotorCtrl_8py.html#aad3a70c11b488eae7ff2488d96831d1f", null ],
    [ "line", "main__MotorCtrl_8py.html#ae0289c712a3478f89dd4ffdfb1f1603d", null ],
    [ "motodrive", "main__MotorCtrl_8py.html#a7e232a2f063a1f030fc579c8863f7120", null ],
    [ "position", "main__MotorCtrl_8py.html#a6b74db73a7047021247d9c7edbc9cfd3", null ],
    [ "ref", "main__MotorCtrl_8py.html#a095ab5c4ce40aed3f621937e9c9db1cc", null ],
    [ "rpArray", "main__MotorCtrl_8py.html#aa583300fcc710e2ead7d02155efe2f14", null ],
    [ "rvArray", "main__MotorCtrl_8py.html#aa7fe51274a217d10863620bdd6048a53", null ],
    [ "t", "main__MotorCtrl_8py.html#a321dc4aff681807c1707dd340653f470", null ],
    [ "task1", "main__MotorCtrl_8py.html#ac62b6254bb8558c0afb4ae07e7843025", null ],
    [ "task2", "main__MotorCtrl_8py.html#a7448fcd1d59ec5217117140238214481", null ],
    [ "time", "main__MotorCtrl_8py.html#a2e1a462d4c24f726042c7b0d5d3a142b", null ],
    [ "v", "main__MotorCtrl_8py.html#adb704a70b77c4e5918e2529c534592b0", null ],
    [ "velocity", "main__MotorCtrl_8py.html#ab867df9f80ed5cd34e477fa07f6e0f10", null ],
    [ "x", "main__MotorCtrl_8py.html#a249fb53c229c2d501844cdf4b2237820", null ]
];