var files_dup =
[
    [ "BlueDrive.py", "BlueDrive_8py.html", [
      [ "BlueDrive", "classBlueDrive_1_1BlueDrive.html", "classBlueDrive_1_1BlueDrive" ]
    ] ],
    [ "BlueUI.py", "BlueUI_8py.html", [
      [ "BlueUser", "classBlueUI_1_1BlueUser.html", "classBlueUI_1_1BlueUser" ]
    ] ],
    [ "ButtonUI.py", "ButtonUI_8py.html", "ButtonUI_8py" ],
    [ "ClosedLoop.py", "ClosedLoop_8py.html", [
      [ "ClosedLoop", "classClosedLoop_1_1ClosedLoop.html", "classClosedLoop_1_1ClosedLoop" ]
    ] ],
    [ "ClosedLoopRef.py", "ClosedLoopRef_8py.html", [
      [ "ClosedLoop", "classClosedLoopRef_1_1ClosedLoop.html", "classClosedLoopRef_1_1ClosedLoop" ]
    ] ],
    [ "ControllerTask.py", "ControllerTask_8py.html", "ControllerTask_8py" ],
    [ "DataGen.py", "DataGen_8py.html", [
      [ "DataGenerator", "classDataGen_1_1DataGenerator.html", "classDataGen_1_1DataGenerator" ]
    ] ],
    [ "DataTask.py", "DataTask_8py.html", "DataTask_8py" ],
    [ "DataUI.py", "DataUI_8py.html", [
      [ "UserInterface", "classDataUI_1_1UserInterface.html", "classDataUI_1_1UserInterface" ]
    ] ],
    [ "ElevatorTasker.py", "ElevatorTasker_8py.html", [
      [ "ElevatorTasker", "classElevatorTasker_1_1ElevatorTasker.html", "classElevatorTasker_1_1ElevatorTasker" ],
      [ "Button", "classElevatorTasker_1_1Button.html", "classElevatorTasker_1_1Button" ],
      [ "MotorDriver", "classElevatorTasker_1_1MotorDriver.html", "classElevatorTasker_1_1MotorDriver" ]
    ] ],
    [ "EncDriver.py", "EncDriver_8py.html", "EncDriver_8py" ],
    [ "EncGlobals.py", "EncGlobals_8py.html", "EncGlobals_8py" ],
    [ "Encoder.py", "Encoder_8py.html", [
      [ "EncoderDriver", "classEncoder_1_1EncoderDriver.html", "classEncoder_1_1EncoderDriver" ]
    ] ],
    [ "EncoderFSM.py", "EncoderFSM_8py.html", [
      [ "EncoderFSM", "classEncoderFSM_1_1EncoderFSM.html", "classEncoderFSM_1_1EncoderFSM" ]
    ] ],
    [ "EncTask.py", "EncTask_8py.html", "EncTask_8py" ],
    [ "ExternalTemp.py", "ExternalTemp_8py.html", "ExternalTemp_8py" ],
    [ "FibonacciCalculator.py", "FibonacciCalculator_8py.html", "FibonacciCalculator_8py" ],
    [ "FrontEnd.py", "FrontEnd_8py.html", "FrontEnd_8py" ],
    [ "FrontEndRef.py", "FrontEndRef_8py.html", "FrontEndRef_8py" ],
    [ "FrontofUI.py", "FrontofUI_8py.html", "FrontofUI_8py" ],
    [ "getChange.py", "getChange_8py.html", "getChange_8py" ],
    [ "IMU.py", "IMU_8py.html", "IMU_8py" ],
    [ "IMUTask.py", "IMUTask_8py.html", "IMUTask_8py" ],
    [ "InternalTemp.py", "InternalTemp_8py.html", "InternalTemp_8py" ],
    [ "main.py", "main_8py.html", null ],
    [ "main_Button.py", "main__Button_8py.html", "main__Button_8py" ],
    [ "main_elevator.py", "main__elevator_8py.html", "main__elevator_8py" ],
    [ "main_Enc.py", "main__Enc_8py.html", "main__Enc_8py" ],
    [ "main_LEDs.py", "main__LEDs_8py.html", "main__LEDs_8py" ],
    [ "main_MotorCtrl.py", "main__MotorCtrl_8py.html", "main__MotorCtrl_8py" ],
    [ "main_Temp.py", "main__Temp_8py.html", "main__Temp_8py" ],
    [ "MotDriver.py", "MotDriver_8py.html", "MotDriver_8py" ],
    [ "MotorCtrl.py", "MotorCtrl_8py.html", [
      [ "Controller", "classMotorCtrl_1_1Controller.html", "classMotorCtrl_1_1Controller" ]
    ] ],
    [ "MotorCtrlRef.py", "MotorCtrlRef_8py.html", [
      [ "Controller", "classMotorCtrlRef_1_1Controller.html", "classMotorCtrlRef_1_1Controller" ]
    ] ],
    [ "MotorDriver.py", "MotorDriver_8py.html", [
      [ "MotoDrive", "classMotorDriver_1_1MotoDrive.html", "classMotorDriver_1_1MotoDrive" ]
    ] ],
    [ "MotorShares.py", "MotorShares_8py.html", "MotorShares_8py" ],
    [ "MotorSharesRef.py", "MotorSharesRef_8py.html", "MotorSharesRef_8py" ],
    [ "MotorUI.py", "MotorUI_8py.html", [
      [ "MotorUser", "classMotorUI_1_1MotorUser.html", "classMotorUI_1_1MotorUser" ]
    ] ],
    [ "MotorUIRef.py", "MotorUIRef_8py.html", [
      [ "MotorUser", "classMotorUIRef_1_1MotorUser.html", "classMotorUIRef_1_1MotorUser" ]
    ] ],
    [ "MotTask.py", "MotTask_8py.html", "MotTask_8py" ],
    [ "ReactionTest.py", "ReactionTest_8py.html", "ReactionTest_8py" ],
    [ "ReactTimer.py", "ReactTimer_8py.html", "ReactTimer_8py" ],
    [ "RealLED.py", "RealLED_8py.html", [
      [ "RLEDTasker", "classRealLED_1_1RLEDTasker.html", "classRealLED_1_1RLEDTasker" ]
    ] ],
    [ "Shared.py", "Shared_8py.html", "Shared_8py" ],
    [ "Shares.py", "Shares_8py.html", "Shares_8py" ],
    [ "TouchDriver.py", "TouchDriver_8py.html", "TouchDriver_8py" ],
    [ "TouchTask.py", "TouchTask_8py.html", "TouchTask_8py" ],
    [ "UIFSM.py", "UIFSM_8py.html", [
      [ "UserInterface", "classUIFSM_1_1UserInterface.html", "classUIFSM_1_1UserInterface" ]
    ] ],
    [ "UITask.py", "UITask_8py.html", "UITask_8py" ],
    [ "Vendotron.py", "Vendotron_8py.html", "Vendotron_8py" ],
    [ "VirtLED.py", "VirtLED_8py.html", [
      [ "VLEDTasker", "classVirtLED_1_1VLEDTasker.html", "classVirtLED_1_1VLEDTasker" ]
    ] ]
];