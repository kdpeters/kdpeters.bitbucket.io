var indexSectionsWithContent =
{
  0: "4_abcdefgijklmnopqrstuvwxyz",
  1: "bcdemrtuv",
  2: "bcdefgimrstuv",
  3: "_abcdefgiklmorstuvwxyz",
  4: "abcdefgijklmnopqrstuvwxyz",
  5: "4bcdefiklmqrstv"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Pages"
};

