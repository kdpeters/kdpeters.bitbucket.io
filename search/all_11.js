var searchData=
[
  ['quadrature_20encoder_20reading_227',['Quadrature Encoder Reading',['../page_Encoder.html',1,'page_final']]],
  ['qballx_228',['qballx',['../Shares_8py.html#a6f72771b89926ab686829221446a917f',1,'Shares']]],
  ['qbally_229',['qbally',['../Shares_8py.html#a0f362fa4093a8010e54277ca76d0fc3d',1,'Shares']]],
  ['qballz_230',['qballz',['../Shares_8py.html#aaeeef30037b938b8fd53fd8f8b2ac1b3',1,'Shares']]],
  ['qdc1_231',['qDC1',['../Shares_8py.html#adf0deed254db2fe58d393cc9c49b30ad',1,'Shares']]],
  ['qdc2_232',['qDC2',['../Shares_8py.html#ab9d7077819725cf7d6dc6c0147bb8265',1,'Shares']]],
  ['qomegx_233',['qomegx',['../Shares_8py.html#abdd10c0ff4e7dfb55751f11d4c779e64',1,'Shares']]],
  ['qomegy_234',['qomegy',['../Shares_8py.html#ac621921d77bbc837b5b72057e4c6729f',1,'Shares']]],
  ['qplatx_235',['qplatx',['../Shares_8py.html#a647696ad64c69e34fb7a23c5c0772e0d',1,'Shares']]],
  ['qplaty_236',['qplaty',['../Shares_8py.html#a39a2b9491f00785ad6eca51239a27e23',1,'Shares']]],
  ['qvelx_237',['qvelx',['../Shares_8py.html#aaaf3bcb4dc1a4ca4903332baa8037438',1,'Shares']]],
  ['qvely_238',['qvely',['../Shares_8py.html#a79dccda2bedd5a13eb1b02cb67f245bf',1,'Shares']]]
];
