var IMU_8py =
[
    [ "BNO055", "classIMU_1_1BNO055.html", "classIMU_1_1BNO055" ],
    [ "angles", "IMU_8py.html#a3e739f627aaa7c1b0b38672346c6d0d1", null ],
    [ "i2c", "IMU_8py.html#a5f6811dfa889d25444b90105e4bf8be7", null ],
    [ "omegas", "IMU_8py.html#afa39bd5469c7600d7d4327d762e6164e", null ],
    [ "pitch", "IMU_8py.html#a68eaa18b0e9e4e1b3565242626415d22", null ],
    [ "pitchrate", "IMU_8py.html#a74c3db410b4b9e5ad5e7e7b79025a29a", null ],
    [ "sensor", "IMU_8py.html#aac1ee9d383b0029b4ffde3fb3a347f58", null ],
    [ "time", "IMU_8py.html#a67b8a7fcc09e3d64a2c198495ca08336", null ]
];