var page_final =
[
    [ "Source Code", "page_final.html#sec_src12", null ],
    [ "Documentation", "page_final.html#sec_doc12", null ],
    [ "Kinematics and Kinetics", "page_HW2.html", null ],
    [ "State Equation Linearization", "page_HW4.html", null ],
    [ "Closed-Loop Gain Analysis", "page_HW5.html", null ],
    [ "Resistive Touch Panel", "page_Touch.html", [
      [ "Testing", "page_Touch.html#sec_Test1", null ],
      [ "Documentation", "page_Touch.html#sec_docTouch", null ]
    ] ],
    [ "DC Motor PWM Operation", "page_Motor.html", [
      [ "Testing", "page_Motor.html#sec_Test2", null ],
      [ "Documentation", "page_Motor.html#sec_docMotor", null ]
    ] ],
    [ "Quadrature Encoder Reading", "page_Encoder.html", [
      [ "Testing", "page_Encoder.html#sec_Test3", null ],
      [ "Documentation", "page_Encoder.html#sec_docEnc", null ]
    ] ],
    [ "Inertial Measurement Unit", "page_IMU.html", [
      [ "Testing", "page_IMU.html#sec_Test4", null ],
      [ "Documentation", "page_IMU.html#sec_docIMU", null ]
    ] ]
];