/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Kyle Peterson ME 305/405 Projects", "index.html", [
    [ "Introduction", "index.html#sec_intro", null ],
    [ "Fibonacci Calculator", "page_fib.html", [
      [ "Source Code", "page_fib.html#sec_src0", null ],
      [ "Documentation", "page_fib.html#sec_doc0", null ]
    ] ],
    [ "Elevator Multi-Tasking", "page_Ele.html", [
      [ "Source Code", "page_Ele.html#sec_src1", null ],
      [ "Documentation", "page_Ele.html#sec_doc1", null ]
    ] ],
    [ "LED Multi-Tasking", "page_leds.html", [
      [ "Source Code", "page_leds.html#sec_src2", null ],
      [ "Documentation", "page_leds.html#sec_doc2", null ]
    ] ],
    [ "Encoder Reading", "page_Enc.html", [
      [ "Source Code", "page_Enc.html#sec_src3", null ],
      [ "Documentation", "page_Enc.html#sec_doc3", null ]
    ] ],
    [ "Serial Encoder Reading", "page_UIENC.html", [
      [ "Source Code", "page_UIENC.html#sec_src4", null ],
      [ "Documentation", "page_UIENC.html#sec_doc4", null ]
    ] ],
    [ "Bluetooth LED Blinking", "page_SmartUI.html", [
      [ "Source Code", "page_SmartUI.html#sec_src5", null ],
      [ "Documentation", "page_SmartUI.html#sec_doc5", null ]
    ] ],
    [ "Motor Control", "page_Moto.html", [
      [ "Speed Profiles", "page_Moto.html#sec_gra", null ],
      [ "Source Code", "page_Moto.html#sec_src6", null ],
      [ "Documentation", "page_Moto.html#sec_doc6", null ]
    ] ],
    [ "Motor Speed/Position Tracking", "page_SpdRef.html", [
      [ "Source Code", "page_SpdRef.html#sec_src7", null ],
      [ "Documentation", "page_SpdRef.html#sec_doc7", null ]
    ] ],
    [ "Vendotron", "page_Vendo.html", [
      [ "Source Code", "page_Vendo.html#sec_src8", null ],
      [ "Documentation", "page_Vendo.html#sec_doc8", null ]
    ] ],
    [ "Reaction Timer", "page_React.html", [
      [ "Source Code", "page_React.html#sec_src9", null ],
      [ "Documentation", "page_React.html#sec_doc9", null ]
    ] ],
    [ "Button Pin Voltage Sampling", "page_ButtVolts.html", [
      [ "Source Code", "page_ButtVolts.html#sec_src10", null ],
      [ "Documentation", "page_ButtVolts.html#sec_doc10", null ]
    ] ],
    [ "Temperature Data Recording via I2C", "page_MCP9808.html", [
      [ "Source Code", "page_MCP9808.html#sec_src11", null ],
      [ "Documentation", "page_MCP9808.html#sec_doc11", null ]
    ] ],
    [ "405 Term Project", "page_final.html", "page_final" ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"BlueDrive_8py.html",
"classBlueUI_1_1BlueUser.html#a0d0e6a285117c90a00368bc2ff42cf57",
"classUIFSM_1_1UserInterface.html#a6241960b88810ab464a2e4f8e6a4a1ec"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';