var annotated_dup =
[
    [ "BlueDrive", null, [
      [ "BlueDrive", "classBlueDrive_1_1BlueDrive.html", "classBlueDrive_1_1BlueDrive" ]
    ] ],
    [ "BlueUI", null, [
      [ "BlueUser", "classBlueUI_1_1BlueUser.html", "classBlueUI_1_1BlueUser" ]
    ] ],
    [ "ClosedLoop", null, [
      [ "ClosedLoop", "classClosedLoop_1_1ClosedLoop.html", "classClosedLoop_1_1ClosedLoop" ]
    ] ],
    [ "ClosedLoopRef", null, [
      [ "ClosedLoop", "classClosedLoopRef_1_1ClosedLoop.html", "classClosedLoopRef_1_1ClosedLoop" ]
    ] ],
    [ "DataGen", null, [
      [ "DataGenerator", "classDataGen_1_1DataGenerator.html", "classDataGen_1_1DataGenerator" ]
    ] ],
    [ "DataUI", null, [
      [ "UserInterface", "classDataUI_1_1UserInterface.html", "classDataUI_1_1UserInterface" ]
    ] ],
    [ "ElevatorTasker", null, [
      [ "Button", "classElevatorTasker_1_1Button.html", "classElevatorTasker_1_1Button" ],
      [ "ElevatorTasker", "classElevatorTasker_1_1ElevatorTasker.html", "classElevatorTasker_1_1ElevatorTasker" ],
      [ "MotorDriver", "classElevatorTasker_1_1MotorDriver.html", "classElevatorTasker_1_1MotorDriver" ]
    ] ],
    [ "EncDriver", null, [
      [ "EncoderDriver", "classEncDriver_1_1EncoderDriver.html", "classEncDriver_1_1EncoderDriver" ]
    ] ],
    [ "Encoder", null, [
      [ "EncoderDriver", "classEncoder_1_1EncoderDriver.html", "classEncoder_1_1EncoderDriver" ]
    ] ],
    [ "EncoderFSM", null, [
      [ "EncoderFSM", "classEncoderFSM_1_1EncoderFSM.html", "classEncoderFSM_1_1EncoderFSM" ]
    ] ],
    [ "ExternalTemp", null, [
      [ "mcp9808", "classExternalTemp_1_1mcp9808.html", "classExternalTemp_1_1mcp9808" ]
    ] ],
    [ "IMU", null, [
      [ "BNO055", "classIMU_1_1BNO055.html", "classIMU_1_1BNO055" ]
    ] ],
    [ "MotDriver", null, [
      [ "DRV8847", "classMotDriver_1_1DRV8847.html", "classMotDriver_1_1DRV8847" ],
      [ "MotorChannel", "classMotDriver_1_1MotorChannel.html", "classMotDriver_1_1MotorChannel" ]
    ] ],
    [ "MotorCtrl", null, [
      [ "Controller", "classMotorCtrl_1_1Controller.html", "classMotorCtrl_1_1Controller" ]
    ] ],
    [ "MotorCtrlRef", null, [
      [ "Controller", "classMotorCtrlRef_1_1Controller.html", "classMotorCtrlRef_1_1Controller" ]
    ] ],
    [ "MotorDriver", null, [
      [ "MotoDrive", "classMotorDriver_1_1MotoDrive.html", "classMotorDriver_1_1MotoDrive" ]
    ] ],
    [ "MotorUI", null, [
      [ "MotorUser", "classMotorUI_1_1MotorUser.html", "classMotorUI_1_1MotorUser" ]
    ] ],
    [ "MotorUIRef", null, [
      [ "MotorUser", "classMotorUIRef_1_1MotorUser.html", "classMotorUIRef_1_1MotorUser" ]
    ] ],
    [ "RealLED", null, [
      [ "RLEDTasker", "classRealLED_1_1RLEDTasker.html", "classRealLED_1_1RLEDTasker" ]
    ] ],
    [ "TouchDriver", null, [
      [ "TouchDriver", "classTouchDriver_1_1TouchDriver.html", "classTouchDriver_1_1TouchDriver" ]
    ] ],
    [ "UIFSM", null, [
      [ "UserInterface", "classUIFSM_1_1UserInterface.html", "classUIFSM_1_1UserInterface" ]
    ] ],
    [ "VirtLED", null, [
      [ "VLEDTasker", "classVirtLED_1_1VLEDTasker.html", "classVirtLED_1_1VLEDTasker" ]
    ] ]
];